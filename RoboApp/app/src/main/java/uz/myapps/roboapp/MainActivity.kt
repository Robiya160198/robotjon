package uz.myapps.roboapp

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.speech.tts.TextToSpeech
import android.speech.tts.UtteranceProgressListener
import android.util.Log
import android.util.Size
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.res.ResourcesCompat
import com.otaliastudios.cameraview.controls.Facing
import husaynhakeem.io.facedetector.Frame
import husaynhakeem.io.facedetector.LensFacing
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(), RecognitionListener, TextToSpeech.OnInitListener {
    companion object {
        private const val TAG = "MainActivity"
        private const val KEY_LENS_FACING = "key-lens-facing"
        private const val REQUEST_RECORD_PERMISSION = 100
        var t = false
        var isSpeech = false
        var isDone = false
    }

    lateinit var tts: TextToSpeech
    var sttIntent: Intent? = null
    var handler = Handler()
    var progressStatus = 0
    private lateinit var speech: SpeechRecognizer
    val list = LoadData()

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_main)
        imageview.background.alpha = 120
        card_btn.background.alpha = 200
        text_speech.background.alpha = 0
        linear.background.alpha = 200
        var id=""
        var id1=Random().nextInt(1000)
        var id2=Random().nextInt(1000)
        var id3=Random().nextInt(1000)
        id=id1.toString()+id2.toString()+id3.toString()
        id_text.text="id: "+id
        ok.setOnClickListener {
            if (password.text.substring(0, 3) == id2.toString()) {
                linear.visibility = View.GONE
                relative.visibility = View.VISIBLE
            }
        }
        tts = TextToSpeech(this, this)
        btn_voice.setOnClickListener {
            isDone = false
            t = true
            start()
            ActivityCompat.requestPermissions(
                this@MainActivity, arrayOf(Manifest.permission.RECORD_AUDIO),
                REQUEST_RECORD_PERMISSION
            )
            tts.stop()
        }

        val lensFacing =
            savedInstanceState?.getSerializable(KEY_LENS_FACING) as Facing? ?: Facing.FRONT
        setupCamera(lensFacing)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_RECORD_PERMISSION -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                progressStatus = 0
                    Thread(Runnable {
                        while (progressStatus < 60) {
                            progressStatus += 1
                            handler.post(Runnable {
                                progressBar.progress = progressStatus
                                Log.d(TAG, "onCreate: $progressStatus")
                            })
                            try {
                                Thread.sleep(50)
                            } catch (e: InterruptedException) {
                                e.printStackTrace()
                            }
                        }
                        Log.d(TAG, "onCreate: time out")
                    }).start()
                progressBar.progress = 0
                spin_kit.visibility = View.VISIBLE
                speech!!.startListening(sttIntent)
            } else {
                Toast.makeText(this@MainActivity, "Permission Denied!", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun start() {
        progressBar.progress = 0
        speech = SpeechRecognizer.createSpeechRecognizer(this)
        Log.i(TAG, "isRecognitionAvailable: " + SpeechRecognizer.isRecognitionAvailable(this))
        speech.setRecognitionListener(this)
        sttIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        sttIntent!!.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        sttIntent!!.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "ru")
        sttIntent!!.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 10)
        Log.d(TAG, "start: $sttIntent")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putSerializable(KEY_LENS_FACING, viewfinder.facing)
        super.onSaveInstanceState(outState)
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setupCamera(lensFacing: Facing) {
        val faceDetector = FaceDetector(faceBoundsOverlay)
        viewfinder.facing = lensFacing
        viewfinder.addFrameProcessor {
            faceDetector.process(
                Frame(
                    data = it.getData(),
                    rotation = it.rotation,
                    size = Size(it.size.width, it.size.height),
                    format = it.format,
                    lensFacing = if (viewfinder.facing == Facing.BACK) LensFacing.BACK else LensFacing.FRONT
                )
            )
        }
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun speakOut(text: String) {
        isSpeech = true
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, "")
        }
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume: ")
        viewfinder.open()
    }

    override fun onPause() {
        super.onPause()
        isSpeech=false
        viewfinder.stopVideo()
    }

    override fun onDestroy() {
        viewfinder.destroy()
        isSpeech=false
        Log.d(TAG, "onDestroy: ")
        super.onDestroy()
    }


    override fun onReadyForSpeech(p0: Bundle?) {
        Log.i(TAG, "onReadyForSpeech: $p0")
    }

    override fun onRmsChanged(p0: Float) {
        Log.i(TAG, "onRmsChanged: $p0")

    }

    override fun onBufferReceived(p0: ByteArray?) {
        Log.d(TAG, "onBufferReceived: $p0")
    }

    override fun onPartialResults(p0: Bundle?) {
        Log.d(TAG, "onPartialResults: $p0")
        if (!isSpeech) speech!!.startListening(sttIntent)
    }

    override fun onEvent(p0: Int, p1: Bundle?) {
    }

    override fun onBeginningOfSpeech() {
        Log.d(TAG, "onBeginningOfSpeech: ")
    }

    override fun onEndOfSpeech() {
        isSpeech=false
        if (!isSpeech) {
            start()
            speech!!.startListening(sttIntent)
        }
    }

    override fun onError(p0: Int) {
        Log.d(TAG, "onError: $p0")
        spin_kit.visibility = View.GONE
        isSpeech=false
//        t = false
//        if (!t) {
//            val randomInt = Random().nextInt(4)
//            speakOut(list.get(list.size - randomInt - 1).answer[0])
//            t = true
//        }
    }

    override fun onResults(p0: Bundle?) {
        Log.d(TAG, "onResults: $p0")
        val matches = p0!!
            .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
        var text = ""
        for (result in matches!!)
            text = result
        spin_kit.visibility = View.GONE
        text_speech.text = text
        Log.i(TAG, "onResults=$text")
        for (i in list) {
            if (text.toLowerCase().contains(i.question)) {
                val randomInt = Random().nextInt(i.answer.size)
                val text = i.answer[randomInt]
                speakOut(text)
                text_speech.text = text
                t = true
            }
        }
        if (!isSpeech) speech!!.startListening(sttIntent)
    }


    override fun onInit(p0: Int) {
        if (p0 == TextToSpeech.SUCCESS) {
            val result = tts!!.setLanguage(Locale("ru"))
            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED)
                Log.d("TTS", "The language specified is not supported!")
            tts.setOnUtteranceProgressListener(object : UtteranceProgressListener() {
                override fun onDone(p0: String?) {
                    isSpeech = false
                    isDone = true


                }

                override fun onError(p0: String?) {
                    isSpeech = false
                }

                override fun onStart(p0: String?) {
                    isSpeech = true
                }

            })
        }
    }
}
